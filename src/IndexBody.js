import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CompunentsHome from './components/Home';
import CompunentsTheLoai from './components/TheLoai';
import CompunentsQuocGia from './components/QuocGia';
import CompunentsPhimLe from './components/PhimLe';
import CompunentsPhimBo from './components/PhimBo'
import CompunentsPhimRap from './components/PhimRap'
import CompunentsTrailer from './components/Trailer'
import CompunentsTopIMDB from './components/TopIMDB'
import Menu from './components/Menu'
class Index extends React.Component {
  render() {
    return (
      <Router>
        <Menu/> 
        <div className="container">
          <Switch>
            <Route path='/' exact component={CompunentsHome} />
            <Route path='/theloai' component={CompunentsTheLoai} />
            <Route path='/quocgia' component={CompunentsQuocGia} />
            <Route path='/phimle' component={CompunentsPhimLe} />
            <Route path='/phimbo' component={CompunentsPhimBo} />
            <Route path='/phimchieurap' component={CompunentsPhimRap} />
            <Route path='/trailer' component={CompunentsTrailer} />
            <Route path='/topimdb' component={CompunentsTopIMDB} />
          </Switch>
        </div>
      </Router>
    )
  }
}
export default Index;