import React from 'react';
import { Link} from 'react-router-dom';
class ItemMenuComponent extends React.Component {
  _renderLayout = this.props.data.map((data, index) => {
    return (
      <Link to={data.a_href} key={index}>{data.type}</Link>
    )
  })
  render() {
    return (
      <li className="dropdown">
        <button className="dropbtn">{this.props.type}</button>
        <div className="dropdown-content">
          {this._renderLayout}
        </div>
      </li>
    )
  }
}
export default ItemMenuComponent;