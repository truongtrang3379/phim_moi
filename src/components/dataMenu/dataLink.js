export const link = ['Thể Loại','Quốc gia','Phim lẻ','Phim bộ']
export const quocgia =[
    {
        type:'Việt Nam',
        a_href:'../quocgia/VietNam'
    },
    {
        type:'Nhật Bản',
        a_href:'../quocgia/NhatBan'
    },
    {
        type:'Pháp',
        a_href:'../quocgia/Phap'
    },
    {
        type:'Canada',
        a_href:'../quocgia/Canada'
    },
    {
        type:'Trung Quốc',
        a_href:'../quocgia/TrungQuoc'
    },
    {
        type:'Hồng Kông',
        a_href:'../quocgia/HongKong'
    },
    {
        type:'Đài Loan',
        a_href:'../quocgia/DaiLoan'
    },
    {
        type:'Mỹ',
        a_href:'../quocgia/My'
    },
    {
        type:'Ấn Độ',
        a_href:'../quocgia/AnDo'
    },
    {
        type:'Úc',
        a_href:'../quocgia/Uc'
    },
    {
        type:'Hàn Quốc',
        a_href:'../quocgia/HanQuoc'
    },
    {
        type:'Thái Lan',
        a_href:'../quocgia/ThaiLan'
    },
    {
        type:'Anh',
        a_href:'../quocgia/Anh'
    },
    {
        type:'Quốc gia khác',
        a_href:'../quocgia/QuocGiaKhac'
    }
]
export const theloai =[
    {
        type:'Phim hành động',
        a_href:'../theloai/hanhdong'
    },
    {
        type:'Phim hài hước',
        a_href:'../theloai/haihuoc'
    },
    {
        type:'Phim chính kịch - Drama',
        a_href:'../theloai/chinhkich_drama'
    },
    {
        type:'Phim viễn tưởng',
        a_href:'../theloai/vientuong'
    },
    {
        type:'Phim võ thuật',
        a_href:'../theloai/vothuat'
    },
    {
        type:'Phim thần thoại',
        a_href:'../theloai/thanthoai'
    },
    {
        type:'Phim Thể thao-Âm nhạc',
        a_href:'../theloai/thethao_amnhac'
    },
    {
        type:'Phim chiến tranh',
        a_href:'../theloai/chientranh'
    },
    {
        type:'Phim kinh dị',
        a_href:'../theloai/kinhdi'
    },
    {
        type:'Phim tâm lý',
        a_href:'../theloai/tamly'
    },
    {
        type:'Phim gia đình',
        a_href:'../theloai/giadinh'
    },
    {
        type:'Phim hình sự',
        a_href:'../theloai/hinhsu'
    },
    {
        type:'Phim hồi hộp-Gây cấn',
        a_href:'../theloai/hoihop_gaycan'
    },
    {
        type:'Phim tài liệu',
        a_href:'../theloai/tailieu'
    },
    {
        type:'Phim hoạt hình',
        a_href:'../theloai/hoathinh'
    },
    {
        type:'Phim phiêu lưu',
        a_href:'../theloai/phieuluu'
    },
    {
        type:'Phim tình cảm-Lãng mạn',
        a_href:'../theloai/tinhcam_langman'
    },
]
export const phimbo = [
    {
        type:'Phim hành động',
        a_href:'../phimbo/hanhdong'
    },
    {
        type:'Phim hài hước',
        a_href:'../phimbo/haihuoc'
    },
    {
        type:'Phim chính kịch - Drama',
        a_href:'../phimbo/chinhkich_drama'
    },
    {
        type:'Phim viễn tưởng',
        a_href:'../phimbo/vientuong'
    },
    {
        type:'Phim võ thuật',
        a_href:'../phimbo/vothuat'
    },
    {
        type:'Phim thần thoại',
        a_href:'../phimbo/thanthoai'
    },
    {
        type:'Phim Thể thao-Âm nhạc',
        a_href:'../phimbo/thethao_amnhac'
    },
    {
        type:'Phim chiến tranh',
        a_href:'../phimbo/chientranh'
    },
    {
        type:'Phim kinh dị',
        a_href:'../phimbo/kinhdi'
    },
    {
        type:'Phim tâm lý',
        a_href:'../phimbo/tamly'
    },
    {
        type:'Phim gia đình',
        a_href:'../phimbo/giadinh'
    },
    {
        type:'Phim hình sự',
        a_href:'../phimbo/hinhsu'
    },
    {
        type:'Phim hồi hộp-Gây cấn',
        a_href:'../phimbo/hoihop_gaycan'
    },
    {
        type:'Phim tài liệu',
        a_href:'../phimbo/tailieu'
    },
    {
        type:'Phim hoạt hình',
        a_href:'../phimbo/hoathinh'
    },
    {
        type:'Phim phiêu lưu',
        a_href:'../phimbo/phieuluu'
    },
    {
        type:'Phim tình cảm-Lãng mạn',
        a_href:'../phimbo/tinhcam_langman'
    },
]
export const phimle = [
    {
        type:'Phim hành động',
        a_href:'../phimle/hanhdong'
    },
    {
        type:'Phim hài hước',
        a_href:'../phimle/haihuoc'
    },
    {
        type:'Phim chính kịch - Drama',
        a_href:'../phimle/chinhkich_drama'
    },
    {
        type:'Phim viễn tưởng',
        a_href:'../phimle/vientuong'
    },
    {
        type:'Phim võ thuật',
        a_href:'../phimle/vothuat'
    },
    {
        type:'Phim thần thoại',
        a_href:'../phimle/thanthoai'
    },
    {
        type:'Phim Thể thao-Âm nhạc',
        a_href:'../phimle/thethao_amnhac'
    },
    {
        type:'Phim chiến tranh',
        a_href:'../phimle/chientranh'
    },
    {
        type:'Phim kinh dị',
        a_href:'../phimle/kinhdi'
    },
    {
        type:'Phim tâm lý',
        a_href:'../phimle/tamly'
    },
    {
        type:'Phim gia đình',
        a_href:'../phimle/giadinh'
    },
    {
        type:'Phim hình sự',
        a_href:'../phimle/hinhsu'
    },
    {
        type:'Phim hồi hộp-Gây cấn',
        a_href:'../phimle/hoihop_gaycan'
    },
    {
        type:'Phim tài liệu',
        a_href:'../phimle/tailieu'
    },
    {
        type:'Phim hoạt hình',
        a_href:'../phimle/hoathinh'
    },
    {
        type:'Phim phiêu lưu',
        a_href:'../phimle/phieuluu'
    },
    {
        type:'Phim tình cảm-Lãng mạn',
        a_href:'../phimle/tinhcam_langman'
    },
]

