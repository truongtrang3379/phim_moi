import React from 'react';
import { quocgia, theloai, phimbo, phimle } from './dataMenu/dataLink';
import { Link } from 'react-router-dom';
import ItemMenuComponent from './dataMenu/loadLink'
const Menu = () => {
    return (
        <div className="row">
            <div className="page-header">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <img src='http://www.phimmoi.com/wp-content/themes/phimmoi/skin/large/img/logo-phimmoicom.png' alt='logo' />
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Tìm kiếm" />
                                <div className="input-group-btn">
                                    <button className="btn btn-default" type="submit">
                                        <i className="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav className="navbar navbar-default">
                    <div className="container">
                        <ul className="nav navbar-nav">
                            <li><Link to="../">PHIM MỚI</Link></li>
                            <ItemMenuComponent type='Thể Loại' data={theloai} />
                            <ItemMenuComponent type='Quốc gia' data={quocgia} />
                            <ItemMenuComponent type='Phim lẻ' data={phimle} />
                            <ItemMenuComponent type='Phim bộ' data={phimbo} />
                            <li><Link to="../phimchieurap">Phim chiếu rạp</Link></li>
                            <li><Link to="../trailer">Trailer</Link></li>
                            <li><Link to="../topimdb">Top IMDB</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>


    )
}
export default Menu;